package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List._
import u03.Lists._
import u02.SumTypes._

class ListTest {


  var list = Cons (10, Cons(20, Cons(30, Nil())))
  val listFold = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test
  def dropOneElement(): Unit ={
    assertEquals(Cons (20 , Cons (30 , Nil () )), drop(list,1))
  }

  @Test
  def dropTwoElements(): Unit ={
    assertEquals(Cons (30 , Nil () ), drop(list,2))
  }

  @Test
  def dropFiveElements(): Unit ={
    assertEquals(Nil(), drop(list,5))
  }

  @Test
  def flatMapAddOne(): Unit ={
    assertEquals(Cons(11,Cons(21,Cons(31,Nil()))), flatMap(list)(v => Cons(v+1, Nil()))) // Cons (11 , Cons (21 , Cons (31 , Nil ()))))
  }

  @Test
  def flatMapAddOneAndTwo(): Unit ={
    assertEquals(Cons (11 , Cons (12 , Cons (21 , Cons (22 , Cons (31 , Cons (32 , Nil ())))))), flatMap (list )(v => Cons ( v +1 , Cons (v +2 , Nil () )))) // Cons (11 , Cons (21 , Cons (31 , Nil ()))))
  }

  @Test
  def testMap(): Unit = {
    assertEquals(
      Cons(Cons(11, Cons(12, Nil())),
      Cons(Cons(21, Cons(22, Nil())),
      Cons(Cons(31, Cons(32, Nil())), Nil()))),
        map(list)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test
  def filterRefactorTen(): Unit ={
    assertEquals(list,filter(list)(_>=10))
  }

  @Test
  def filterRefactorTwentyOne(): Unit ={
    assertEquals(Cons(30, Nil()),filter(list)(_>21))
  }

  @Test
  def filterRefactorThirtyOne(): Unit ={
    assertEquals(Nil(),filter(list)(_>31))
  }

  import u02.Optionals._
  @Test
  def maxTest(): Unit ={
    assertEquals(Option.Some(30),max(list))
  }

  @Test
  def maxTestNil(): Unit ={
    assertEquals(Option.None(),max(Nil()))
  }


  @Test
  def testPersonStudent(): Unit ={
    val Carlo = Student("Carlo",97)
    val persons: List[Person] = Cons(Carlo,Nil())
    assertEquals(Nil(),teacherCourses(persons))
  }

  @Test
  def testPersonTeacher(): Unit ={
    val Carlo = Teacher("Carlo","PPDS")
    val persons: List[Person] = Cons(Carlo,Nil())
    assertEquals(Cons("PPDS",Nil()),teacherCourses(persons))
  }

  @Test
  def testPersonTeacherAndStudent(): Unit ={
    val Carlo = Teacher("Carlo","PPDS")
    val Carlito = Student("Carlito",17);
    val persons: List[Person] = Cons(Carlo,Cons(Carlito,Nil()))
    assertEquals(Cons("PPDS",Nil()),teacherCourses(persons))
  }

  @Test
  def testLeftFold: Unit ={
    assertEquals(-16, foldLeft(listFold)(0)(_-_))
  }

 @Test
  def testReverse(): Unit ={
    assertEquals(Cons(5, Cons(1, Cons(7, Cons(3, Nil())))), reverse(listFold))
  }

  @Test
  def testRightFold: Unit ={
    assertEquals(-8, foldRight(listFold)(0)(_-_))
  }

}
