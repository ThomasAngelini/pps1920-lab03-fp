package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List._
import u03.Streams._
import u03.Streams.Stream._

class StreamTest {

  val s = Stream.take(Stream.iterate(0)(_+1))(10)

  @Test
  def testDrop(): Unit ={
    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),Stream.toList(Stream.drop(s)(6)))
  }

  @Test
  def testConstant(): Unit ={
    assertEquals(Cons("x",Cons("x",Cons("x",Cons("x",Cons("x",Nil()))))),Stream.toList(Stream.take(constant("x"))(5)))
  }

  @Test
  def testFib(): Unit ={
    val fibs: Stream[Int] = Stream.fibs//(0)(0)
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))),
      Stream toList Stream.take(fibs)(8))
  }


}
