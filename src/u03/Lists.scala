package u03

import com.sun.jnlp.PersistenceServiceNSBImpl


object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    /*def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }*/

    /*def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }*/

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, tail) if n > 0 => drop(tail, n - 1)
      case l if (n == 0 || l == Nil()) => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(head, tail) => append(f(head), flatMap(tail)(f))
      case Nil() => Nil()
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(v => Cons(mapper(v), Nil()))

    def filter[A](l: List[A])(pred: A => Boolean): List[A] = flatMap(l)(v => pred(v) match {
      case true => Cons(v, Nil())
      case false => Nil()
    })

    import u02.Optionals._

    def max(l: List[Int]): Option[Int] = l match {
      case Cons(head, tail) => _max(Option.Some(head), max(tail))
      case _ => Option.None();
    }

    def _max(e1: Option[Int], e2: Option[Int]): Option[Int] = Option.getOrElse(e1, 0) > Option.getOrElse(e2, 0) match {
      case true => e1;
      case false => e2;
    }

    import u02.SumTypes._

    def teacherCourses(persons: List[Person]): List[String] = flatMap(persons) {
      case Teacher(_, course) => Cons(course, Nil())
      case _ => Nil()
    }

    def foldLeft[A, B](list: List[A])(default: B)(acc: (B, A) => B): B = list match {
      case Cons(elem, tail) => foldLeft(tail)(acc(default, elem))(acc)
      case _ => default
    }

    def reverse[A](lists: List[A]): List[A] = lists match {
        case Cons(head, tail) => append(reverse(tail), Cons(head,Nil()))
        case _ => Nil()
    }

    def foldRight[A, B](list: List[A])(default: B)(acc: (A, B) => B): B =
      foldLeft(reverse(list))(default)((default,elem) => acc(elem,default))
  }
}

/*object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  import u03.Lists.List
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
}*/